#include "SDL2/SDL.h"
#include <vector>

class Puyo {
	public:
		Puyo();
		Puyo(int x, int y, SDL_Texture* text, int type);
		~Puyo();
		void draw();
		SDL_Texture* texture;
		SDL_Rect rectangle;
		SDL_Renderer* renderer;
		int type;
		bool shouldTriggerSpawn;
		bool canMoveDown;
		bool canMoveLeft;
		bool canMoveRight;
		bool canWarpLeft;
		bool canWarpRight;
		bool puyoAbove;
		bool horizontalTag;
		bool verticalTag;
		bool certainDelete;
		int horizontalChain;
		int verticalChain;
};