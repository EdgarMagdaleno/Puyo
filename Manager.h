#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"
#include "Globals.h"
#include <vector>
#include <iostream>

class Manager {
	public:
		static Manager* getInstance();
		SDL_Window* window;
		SDL_Renderer* renderer;
		SDL_Texture* background;
		SDL_Texture* wall;
		SDL_Texture* floor;
		SDL_Texture* gameOverTexture;
		std::vector<SDL_Texture*> textures;
		void start();
		bool exit();

	private:
		static Manager* instance;
		SDL_Event event;
		SDL_Texture* loadTexture(std::string path);
		void createWindow(const char* title);
		void createRenderer();
		void printError(std::string msg);
		void loadResources();
};