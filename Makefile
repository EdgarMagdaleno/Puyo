OBJS = *.cpp *.h
COMPILER = g++
COMPILER_FLAGS = -std=gnu++11 -w

UNAME := $(shell uname)
ifeq ($(UNAME), MSYS_NT-10.0)
	LINKER_FLAGS = -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf -static-libgcc -static-libstdc++
endif

ifeq ($(UNAME), Linux)
	LINKER_FLAGS = -lSDL2 -lSDL2_image -lSDL2_ttf
endif

OBJ_NAME = game

all : $(OBJS)
	$(COMPILER) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)